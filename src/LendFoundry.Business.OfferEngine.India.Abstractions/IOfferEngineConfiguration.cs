﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Business.OfferEngine.India
{
    public interface IOfferEngineConfiguration : IDependencyConfiguration    {
      
        string DurationType { get; set; }
        double OriginatingFeeMinLoanAmount { get; set; }
        double FixedOriginatingFee { get; set; }
        double OriginatingFeePer { get; set; }
        double ACHFee { get; set; }
        string DefaultSource { get; set; }
        string ConnectionString { get; set; }
    }
}
