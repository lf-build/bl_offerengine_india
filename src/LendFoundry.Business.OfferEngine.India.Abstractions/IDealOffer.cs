﻿using LendFoundry.Foundation.Date;
using LendFoundry.ProductConfiguration;

namespace LendFoundry.Business.OfferEngine.India {
    public interface IDealOffer {
        double LoanAmount { get; set; }
        double RepaymentAmount { get; set; } //payBackAmount
        double SellRate { get; set; } //rate
        double CommissionRate { get; set; }
        double CommissionAmount { get; set; }
        string DurationType { get; set; }
        decimal TermPayment { get; set; }
        decimal AmountFunded { get; set; }
        string BankName { get; set; }
        string AccountNumber { get; set; }
        string AccountType { get; set; }
        int Term { get; set; }
        //new added
        double Comp { get; set; }
        double MaxGross { get; set; }
        double ProcessingFees { get; set; }
        string Comments { get; set; }
        string Program { get; set; }
        // new added 
        double ApprovedAmount { get; set; }
        string TypeOfPayment { get; set; }
        double PaymentAmount { get; set; }
        int NumberOfPayment { get; set; }
        double OriginatingFeeAmount { get; set; } //PSF fee
        double ACHFee { get; set; }
        double BuyRate { get; set; }
        double LenderReturn { get; set; }
        double NetFundingRate { get; set; }
        double AfterDefaultAssumption { get; set; }
        int AverageLife { get; set; }
        double IRR { get; set; }
        string Grade { get; set; }
        string CreatedBy { get; set; }
        TimeBucket CreatedOn { get; set; }
        string OfferId { get; set; }
        string LenderId { get; set; }
        double ExpectedYield { get; set; }
        string PaymentDay { get; set; }
        string IFSC { get; set; }
        string MICR { get; set; }
         double AnnualRate { get; set; }
         int Tenure { get; set; }
         int NumberofPayments { get; set; }

         PortfolioType ProductPortfolioType{get;set;}
         ProductCategory ProductCategory{get;set;}
         double MinimumAmountDuePercentage { get; set; }   



    }
}