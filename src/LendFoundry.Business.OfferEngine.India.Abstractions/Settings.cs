﻿using System;

namespace LendFoundry.Business.OfferEngine.India
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-offerengine-india";


    }
}