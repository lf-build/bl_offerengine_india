﻿
namespace LendFoundry.Business.OfferEngine.India
{
    public interface IComputedOffer
    {
        int term { get; set; }
        double sell_rate { get; set; }
        string pti { get; set; }
        double monthly_revenue { get; set; }
        double daily_revenue { get; set; }
        double max_daily_debt_obligation { get; set; }
        double max_debt_payment { get; set; }
        double cal_loan_offer { get; set; }
        double loan_offer { get; set; }
        double buy_rate { get; set; }
    }
}