﻿using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine.India
{
    public interface IBusinessLoanOffer
    {
        string OfferId { get; set; }
        double MinAmount { get; set; }
        double MaxAmount { get; set; }
        double MinFactor { get; set; }
        double MaxFactor { get; set; }
        int MinDuration { get; set; }
        int MaxDuration { get; set; }
        string DurationType { get; set; }
        IList<IOfferFee> OfferFees { get; set; }
        //new added
        double Comp { get; set; }
        double MaxGross { get; set; }
        double PSF { get; set; }
        double OriginatingFeeAmount { get; set; }
        double MinDailyPayment { get; set; }
        double MaxDailyPayment { get; set; }
        string Grade { get; set; }
        bool IsManual { get; set; }
        double LoanAmountCap { get; set; }
    }
}