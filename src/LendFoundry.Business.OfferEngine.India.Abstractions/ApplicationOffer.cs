﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine.India
{
    ///
    /// API object
    ///
    public class ApplicationOffer : Aggregate, IApplicationOffer
    {
        public ApplicationOffer()
        {
        }

        /// 
        /// ## Entity Type ##
        /// Type of entity
        /// 
        public string EntityType { get; set; }
        /// 
        /// ## Entity Id ##
        /// Id of the entity
        /// 
        public string EntityId { get; set; }
        /// 
        /// ## Offers ##
        /// The list of Offers
        /// 
        [JsonConverter(typeof(InterfaceListConverter<IBusinessLoanOffer, BusinessLoanOffer>))]
        public List<IBusinessLoanOffer> Offers { get; set; }
        /// 
        /// ## Deal Offer ##
        /// The deal offer
        /// 
        [JsonConverter(typeof(InterfaceConverter<IDealOffer, DealOffer>))]
        public IDealOffer DealOffer { get; set; }
        /// 
        /// ## Lender Deals ##
        /// The list of lender deals
        /// 
        [JsonConverter(typeof(InterfaceListConverter<IDealOffer, DealOffer>))]
        public List<IDealOffer> LenderDeals { get; set; }
        /// 
        /// ## Monthly Revenue - Amount ##
        /// The monthly revenue
        /// 
        public double MonthlyRevenue { get; set; }
        /// 
        /// ## Average Daily Balance - Amount ##
        /// The average daily balance
        /// 
        public double AverageDailyBalance { get; set; }
        /// 
        /// ## Source ##
        /// The source
        /// 
        public string Source { get; set; }
        /// 
        /// ## Created By ##
        /// User that created this
        /// 
        public string CreatedBy { get; set; }
        /// 
        /// ## Created On ##
        /// Timestamp of creation
        /// 
        public TimeBucket CreatedOn { get; set; }
        /// 
        /// ## Deal Offers ##
        /// List containing history of deals
        /// 
        [JsonConverter(typeof(InterfaceListConverter<IDealOffer, DealOffer>))]
        public List<IDealOffer> DealHistory { get; set; }
        /// 
        /// ## Funder ##
        /// Details of Funder
        /// 
        public string Funder { get; set; }
        /// 
        /// ## Product ID ##
        /// ID of product
        /// 
        public string ProductId { get; set; }
        /// 
        /// ## Program ##
        /// The program
        /// 
        public string Program { get; set; }
        /// 
        /// ## Submitted Date ##
        /// The date of submission
        /// 
        public double SubmittedDate { get; set; }
        /// 
        /// ## When Approved ##
        /// Date and time when approved
        /// 
        public double ApprovedDateTime { get; set; }
    }
}