﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine.India
{
    public class DealOfferRequest : IDealOfferRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IDealOffer, DealOffer>))]
        public List<IDealOffer> dealOffers { get; set; }
        public string Source { get; set; }

    }
}