﻿using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Business.OfferEngine.India.Api.Controllers
{
    /// <summary>
    /// OfferEngineController
    /// </summary>
    [Route("/")]
    public class OfferEngineController : ExtendedController
    {
        #region Constructors
        /// <summary>
        /// OfferEngineController
        /// </summary>
        /// <param name="service"></param>

        public OfferEngineController(IOfferEngineService service)
        {
            OfferEngineService = service;
        }

        #endregion Constructors

        #region Private Properties
               
        private IOfferEngineService OfferEngineService { get; }

        #endregion Private Properties

        #region Public Methods
        /// <summary>
        /// GetApplicationOffers
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpGet("/{entityType}/{entityId}/offerengine/getapplicationoffers")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetApplicationOffers(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetApplicationOffers(entityType, entityId))));
        }
        /// <summary>
        /// AddDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpPost("/{entityType}/{entityId}/offerengine/deal")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddDeal(string entityType, string entityId, [FromBody]DealOfferRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.AddDeal(entityType, entityId, request))));
        }
        /// <summary>
        /// AddLenderDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>

        [HttpPost("/{entityType}/{entityId}/offerengine/lender/deal")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddLenderDeal(string entityType, string entityId, [FromBody]DealOfferRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.AddLenderDeal(entityType, entityId, request))));
        }


        
       
         ///<summary>
         ///Save Offers
         ///</summary>
         /// <param name="entityType">entityType is required</param>
         /// <param name="entityId">entityType is required</param>
         /// <param name="offers">
         /// ## ApplicationOffer ##
         /// <table>
         /// <tr><th>Field Name</th><th>Data Type </th><th>Mandatory?</th><th>Validations</th><th>Description</th></tr>
         /// <tr><td><code>EntityType</code></td><td>String</td><td>Mandatory</td><td>Required</td><td>Type of entity</td></tr>
         /// <tr><td><code>EntityId</code></td><td>String</td><td>Mandatory</td><td>Required</td><td>Id of the entity</td></tr>
         /// <tr><td><code><b>Offers</b></code></td><td>List of <code>IBusinessLoanOffer</code>s</td><td></td><td></td><td>See <code>IBusinessLoanOffer</code> documentation</td></tr>
         /// <tr><td><code><b>DealOffer</b></code></td><td>List of <code>IDealOffer</code>s</td><td></td><td></td><td>See <code>IDealOffer</code> documentation</td></tr>
         /// <tr><td><code><b>LenderDetails</b></code></td><td>List of <code>IDealOffer</code>s</td><td></td><td></td><td>See <code>IDealOffer</code> documentation</td></tr>
         /// <tr><td><code>MonthlyRevenue</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>AverageDailyBalance</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>Source</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>CreatedBy</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>CreatedOn</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code><b>DealHistory</b></code></td><td>List of <code>IDealOffer</code>s</td><td></td><td></td><td>See <code>IDealOffer</code> documentation</td></tr>
         /// <tr><td><code>Funder</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>ProductId</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>Program</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>SubmittedDate</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>ApprovedDateTime</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// </table>
         /// <h3>IBusinessLoanOffer</h3>
         /// <table>
         /// <tr><th><code>Field Name</code></th><th>Data Type </th><th>Mandatory?</th><th>Validations</th><th>Description</th></tr>
         /// <tr><td><code>OfferId</code></td><td>String</td><td>optional</td><td>No validations</td><td>The id of the offer</td></tr>
         /// <tr><td><code>MinAmount</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>The minimum amount of the offer</td></tr>
         /// <tr><td><code>MaxAmount</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>The maximum amount of the offer</td></tr>
         /// <tr><td><code>MinFactor</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>The minimum factor</td></tr>
         /// <tr><td><code>MaxFactor</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>The maximum factor</td></tr>
         /// <tr><td><code>MinDuration</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The minimum duration</td></tr>
         /// <tr><td><code>MaxDuration</code></td><td>Integer</td><td>optional</td><td>No validations</td><td>The maximum duration</td></tr>
         /// <tr><td><code>DurationType</code></td><td>String</td><td>optional</td><td>No validations</td><td>The type of duration</td></tr>
         /// <tr><td><code><b>OfferFees</b></code></td><td>List of <code>IOfferFee</code>s</td><td>optional</td><td>If present must be a list of <code>OfferFee</code> objects</td><td>See <code>IOfferFee</code> documentation</td></tr>
         /// <tr><td><code>Comp</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>Comp</td></tr>
         /// <tr><td><code>MaxGross</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>Max Gross</td></tr>
         /// <tr><td><code>PSF</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>PSF</td></tr>
         /// <tr><td><code>OriginatingFeeAmount</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>Originating Fee Amount</td></tr>
         /// <tr><td><code>MinDailyPayment</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>Minimum Daily Payment</td></tr>
         /// <tr><td><code>MaxDailyPayment</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>Maximum Daily Payment</td></tr>
         /// <tr><td><code>Grade</code></td><td>String</td><td>optional</td><td>No Validations</td><td>Grade</td></tr>
         /// <tr><td><code>IsManual</code></td><td>boolean</td><td>optional</td><td>No Validations</td><td>set to true if it is manual offer, false if it is an auto generated offer</td></tr>
         /// <tr><td><code>LoanAmountCap</code></td><td>Decimal</td><td>optional</td><td>No Validations</td><td>The upper limit for the loan amount</td></tr>
         /// </table>
         /// 
         /// <h3>IOfferFee</h3>
         /// <table>
         /// <tr><th><code>Field Name</code></th><th>Data Type </th><th>Mandatory?</th><th>Validations</th><th>Description</th></tr>
         /// <tr><td><code>FeeAmount</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>The amount of the fee</td></tr>
         /// <tr><td><code>FeeType</code></td><td>list of values</td><td>optional</td><td>Must be one of <code>OneTime</code>, <code>Recurring</code> or <code>PercentageOfLoanAmount</code></td><td>The type of fee</td></tr>
         /// <tr><td><code>FeePercentage</code></td><td>Decimal</td><td>optional</td><td>No validations</td><td>Percentage of fees</td></tr>
         /// <tr><td><code>IsIncludedInLoanAmount</code></td><td>boolean</td><td>optional</td><td>No validations</td><td>Loan Amount</td></tr>
         /// </table>
         /// 
         /// <h3>IDealOffer</h3>
         /// <table>
         /// <tr><th><code>Field Name</code></th><th>Data Type </th><th>MandatoryNo Validations</th><th>Validations</th><th>Description</th></tr>
         /// <tr><td><code>LoanAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The amount of the loan</td></tr>
         /// <tr><td><code>RepaymentAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The repayment amount of the loan</td></tr>
         /// <tr><td><code>SellRate</code></td><td>Decimal</td><td>Optional</td><td>Factor rate value , Should be 0 for termloan and scf</td><td>The sell rate of the loan</td></tr>
         /// <tr><td><code>CommissionRate</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The commission rate of the loan</td></tr>
         /// <tr><td><code>CommissionAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The commission amount of the loan</td></tr>
         /// <tr><td><code>DurationType</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The type of duration</td></tr>
         /// <tr><td><code>TermPayment</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The term payment</td></tr>
         /// <tr><td><code>AmountFunded</code></td><td>decimal</td><td>Optional</td><td>No Validations</td><td>The funded amount</td></tr>
         /// <tr><td><code>BankName</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The bank name</td></tr>
         /// <tr><td><code>AccountNumber</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The account number</td></tr>
         /// <tr><td><code>AccountType</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The account type</td></tr>
         /// <tr><td><code>IFSC</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The IFSC code</td></tr>
         /// <tr><td><code>MICR</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The MICR code</td></tr>
         /// <tr><td><code>Term</code></td><td>Integer</td><td>Optional</td><td>No Validations</td><td>The term of the loan</td></tr>
         /// <tr><td><code>Comp</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>Comp</td></tr>
         /// <tr><td><code>MaxGross</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The max gross</td></tr>
         /// <tr><td><code>ProcessingFees</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The processing fees</td></tr>
         /// <tr><td><code>Comments</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>comments</td></tr>
         /// <tr><td><code>ApprovedAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The approved amount</td></tr>
         /// <tr><td><code>TypeOfPayment</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The type of payment</td></tr>
         /// <tr><td><code>PaymentAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The payment amount</td></tr>
         /// <tr><td><code>NumberOfPayment</code></td><td>Integer</td><td>Optional</td><td>No Validations</td><td>The original fee amount</td></tr>
         /// <tr><td><code>OriginatingFeeAmount</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The originating fee amount</td></tr>
         /// <tr><td><code>ACHFee</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The ACH fee</td></tr>
         /// <tr><td><code>BuyRate</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The buy rate</td></tr>
         /// <tr><td><code>LenderReturn</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The lender return</td></tr>
         /// <tr><td><code>NetFundingRate</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The net funding rate</td></tr>
         /// <tr><td><code>AfterDefaultAssumption</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The after default assumption</td></tr>
         /// <tr><td><code>AverageLife</code></td><td>Integer</td><td>Optional</td><td>No Validations</td><td>The average life</td></tr>
         /// <tr><td><code>IRR</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The internal rate of return</td></tr>
         /// <tr><td><code>Grade</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The grade</td></tr>
         /// <tr><td><code>CreatedBy</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>Creator of the offer</td></tr>
         /// <tr><td><code>CreatedOn</code></td><td>Timestamp</td><td>Optional</td><td>No Validations</td><td>Timestamp of offer creation</td></tr>
         /// <tr><td><code>OfferId</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The offer ID</td></tr>
         /// <tr><td><code>LenderId</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The lender ID</td></tr>
         /// <tr><td><code>ExpectedYield</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The expected yield</td></tr>
         /// <tr><td><code>PaymentDay</code></td><td>String</td><td>Optional</td><td>No Validations</td><td>The payment day</td></tr>
         /// <tr><td><code>AnnualRate</code></td><td>Decimal</td><td>Mandatory</td><td>Should be 0 for MCA</td><td>The annual rate</td></tr>
         /// <tr><td><code>Tenure</code></td><td>Integer</td><td>Optional</td><td>Should be 0 for MCA and for scf</td><td>The tenure of the loan</td></tr>
         /// <tr><td><code>NumberOfPayments</code></td><td>Integer</td><td>Optional</td><td>Should be 0 for termloan and scf</td><td>The number of payments</td></tr>
         /// <tr><td><code>ProductPortfolioType</code></td><td>String</td><td>Optional</td><td>Valid values are <code>Installment</code>, <code>Mortgage</code>, <code>LineOfCredit</code>, <code>Open</code>, <code>Revolving</code>, <code>MCA</code>, <code>MCALOC</code>, <code>SCF</code></td><td>The product portfolio type</td></tr>
         /// <tr><td><code>ProductCategory</code></td><td>String</td><td>Optional</td><td>Valid values are <code>Individual</code> and <code>Business</code></td><td>The product category</td></tr>
         /// <tr><td><code>MinimumAmountDuePercentage</code></td><td>Decimal</td><td>Optional</td><td>No Validations</td><td>The minimum amount due percentage</td></tr>
         /// </table>
        /// </param>
         /// <returns></returns>
         /// <exception cref="ArgumentNullException"></exception>
        [HttpPost("/{entityType}/{entityId}/offerengine/saveoffer")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SaveOffers(string entityType, string entityId, [FromBody]ApplicationOffer offers)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.SaveOffer(entityType, entityId, offers))));
        }
        /// <summary>
        /// GetDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/deal")]
        [ProducesResponseType(typeof(IDealOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetDeal(entityType, entityId))));
        }
        /// <summary>
        /// GetLenderDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>

        [HttpGet("/{entityType}/{entityId}/lender/deal")]
        [ProducesResponseType(typeof(IDealOffer[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLenderDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetLenderDeal(entityType, entityId))));
        }
        /// <summary>
        /// DeleteOffersAndDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>

        [HttpDelete("/{entityType}/{entityId}/delete/offers")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteOffersAndDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.DeleteOffersAndDeal(entityType, entityId))));
        }

        #endregion Public Methods
    }
}