﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Business.OfferEngine.India.Client
{
    public class OfferEngineServiceClientFactory : IOfferEngineServiceClientFactory
    {
        #region Constructors
        [Obsolete("Need to use the overloaded with Uri")]
        public OfferEngineServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
       
        public OfferEngineServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }

        #endregion

        #region Public Methods
        public IOfferEngineService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("business_offerengine_india");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new OfferEngineService(client);

        }
        #endregion
    }
}
