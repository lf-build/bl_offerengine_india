﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.OfferEngine.India.Client
{
    public class OfferEngineService : IOfferEngineService
    {
        #region Constructors

        public OfferEngineService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IApplicationOffer> ComputeOffer(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/offerengine/computeoffer", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<ApplicationOffer>(request);
        }

        public async Task<IApplicationOffer> AddDeal(string entityType, string entityId, IDealOfferRequest request)
        {
            var objRequest = new RestRequest("/{entityType}/{entityId}/offerengine/deal", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<IApplicationOffer> AddLenderDeal(string entityType, string entityId, IDealOfferRequest request)
        {
            var objRequest = new RestRequest("/{entityType}/{entityId}/offerengine/lender/deal", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<IApplicationOffer> GetApplicationOffers(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/offerengine/getapplicationoffers", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<ApplicationOffer>(request);
        }

        public async Task<IApplicationOffer> SaveOffer(string entityType, string entityId, IApplicationOffer offers)
        {
            var request = new RestRequest("/{entityType}/{entityId}/offerengine/saveoffer", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(offers);
            return await Client.ExecuteAsync<ApplicationOffer>(request);
        }

        public async Task<IDealOffer> GetDeal(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/deal", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<DealOffer>(request);
        }

        public async Task<List<IDealOffer>> GetLenderDeal(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/lender/deal", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<List<IDealOffer>>(request);
        }

        public async Task<bool> DeleteOffersAndDeal(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/delete/offers", Method.DELETE);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<bool>(request);
        }

        #endregion Public Methods
    }
}