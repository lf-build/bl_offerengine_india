﻿using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Business.OfferEngine.India.Client
{
    public static class OfferEngineServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddOfferEngineService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IOfferEngineServiceClientFactory>(p => new OfferEngineServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IOfferEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddOfferEngineService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IOfferEngineServiceClientFactory>(p => new OfferEngineServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IOfferEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddOfferEngineService(this IServiceCollection services)
        {
            services.AddSingleton<IOfferEngineServiceClientFactory>(p => new OfferEngineServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IOfferEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
