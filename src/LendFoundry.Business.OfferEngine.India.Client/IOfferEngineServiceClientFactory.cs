﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine.India.Client
{
    public interface IOfferEngineServiceClientFactory
    {
        IOfferEngineService Create(ITokenReader reader);
    }
}
